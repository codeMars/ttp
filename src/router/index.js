import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'

/* Layout */
import Layout from '@/layout/Layout' //引入想要的组件


Vue.use(Router)


export const constantRouterMap = [

    {
        path: '/login',
        name: '登录',
        hidden: true,
        component: () =>
            import ('@/views/login/login'),
    },



    { //每一个链接都是一个对象
        path: '/', //链接路径
        component: Layout, //对应的组件模板
        name: '首页',
        hidden: false,
        redirect: '/dashboard', //重定向
        children: [{
            path: 'dashboard',
            hidden: false,
            component: () =>
                import ('@/views/home/home'),
            name: '首页', //路由名称
            meta: {}
        }, ]

    },

    { //每一个链接都是一个对象
        path: '/project', //链接路径
        component: Layout, //对应的组件模板
        name: '项目管理',
        hidden: false,
        redirect: '/project', //重定向
        children: [{
            path: 'project',
            name: '项目管理',
            hidden: false,
            component: () =>
                import ('@/views/interface/project/projectManager'),
        }, ]

    },


    { //每一个链接都是一个对象
        path: '/interface', //链接路径
        component: Layout, //对应的组件模板
        name: '接口管理',
        hidden: false,
        redirect: '/interfaceManager', //重定向
        children: [{
                path: 'interfaceManager',
                name: '接口管理',
                hidden: false,
                component: () =>
                    import ('@/views/interface/interface/interfaceManager'),
            },
            {
                path: 'interfaceEdiet',
                name: '接口分组',
                hidden: false,
                component: () =>
                    import ('@/views/interface/interface/interfaceManager'),
            },
        ]
    },

    {
        path: '/interface/:interface_id',
        component: Layout, //对应的组件模板
        name: '接口预览',
        hidden: false,
        redirect: '/interface/:interface_id', //重定向
        children: [{
                path: '/interface/:interface_id',
                name: '接口管理',
                hidden: false,
                component: () =>
                    import ('@/views/interface/interface/interface'),
            },

        ]
    },



    { //每一个链接都是一个对象
        path: '/function', //链接路径
        component: Layout, //对应的组件模板
        name: '功能菜单',
        hidden: false,
        redirect: '/directives', //重定向
        children: [

            {
                path: 'directives',
                hidden: false,
                component: () =>
                    import ('@/views/baseFunction/directives'),
                name: '常用指令', //路由名称
            },
            {
                path: 'lifeCycle',
                hidden: false,
                component: () =>
                    import ('@/views/baseFunction/lifeCycle'),
                name: '生命周期', //路由名称
            },
            {
                path: 'dynamicCSS',
                hidden: false,
                component: () =>
                    import ('@/views/baseFunction/dynamicCSS'),
                name: '动态样式绑定', //路由名称
            },
            {
                path: 'watch',
                hidden: false,
                component: () =>
                    import ('@/views/baseFunction/watch'),
                name: '倾听器', //路由名称
            },
            {
                path: 'calculationProperties',
                hidden: false,
                component: () =>
                    import ('@/views/baseFunction/calculationProperties'),
                name: '计算属性',

            },
            {
                path: 'template',
                hidden: false,
                component: () =>
                    import ('@/views/baseFunction/template'),
                name: '组件通信',

            },



        ]

    },
]


export default new Router({
    // mode: 'hash',
    mode: 'history',
    routes: constantRouterMap
})