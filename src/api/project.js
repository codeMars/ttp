import request from "@/utils/request";


export function getProject(params) {
    return request({
        url: '/project/listAll',
        method: 'get',
        data: params
    })
}


export function addProject(params) {
    return request({
        url: '/project/create',
        method: 'post',
        data: params,
    })
}