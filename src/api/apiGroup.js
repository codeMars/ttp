import request from "@/utils/request";


//添加分组
export function addApiGroup(data) {
    return request({
        url: '/apigroup/create',
        method: 'post',
        data: data,
    })

}

//获取项目分组
export function getApiGroup(projectId) {
    return request({
        url: '/apigroup/list',
        method: 'get',
        params: {
            projectId
        }
    })

}

//删除分组
export function deleteApiGroup(id) {
    return request({
        url: `/apigroup/delete/${id}`,
        method: 'delete',
    })
}

//更新分组
export function updateApiGroup(data) {
    return request({
        url: '/apigroup/update',
        method: 'post',
        data: data,
    })

}