import request from "@/utils/request";

//添加接口
export function addApi(data) {
    return request({
        url: '/api/create',
        method: 'post',
        data: data
    })

}

//获取接口信息
export function getApi(projectId, apiGroupId, id, pageIndex, pageSize) {
    return request({
        url: '/api/list',
        method: 'get',
        params: {
            projectId,
            apiGroupId,
            id,
            pageIndex,
            pageSize
        }

    })
}


//删除接口
export function deleteApi(id) {
    return request({
        url: `/api/delete/${id}`,
        method: 'delete',
    })

}

//更新接口
export function updateApi(data) {
    return request({
        url: '/api/update',
        method: 'post',
        data: data
    })

}