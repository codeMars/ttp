import Vue from "vue";
import Vuex from "Vuex";

Vue.use(Vuex)



const store = new Vuex.Store({
    state: {
        //项目id
        projectId: localStorage.getItem('projectId') || 0,

        //项目名
        projectName: localStorage.getItem('projectName') || '',

        //分组id
        apiGroupId: localStorage.getItem('apiGroupId') || 0,

        //项目分组
        apiGroup: [],

        //接口信息
        interfaceInfo: {}


    },

    mutations: {
        //项目id
        SET_PROJECTID: (state, projectId) => {
            state.projectId = projectId
            localStorage.setItem('projectId', projectId)
        },

        //项目名
        SET_PROJECTNAME: (state, projectName) => {
            state.projectName = projectName
            localStorage.setItem('projectName', projectName)
        },
        //分组id
        SET_APIGROUPID: (state, apiGroupId) => {
            state.apiGroupId = apiGroupId
            localStorage.setItem('apiGroupId', apiGroupId)
        },

        //项目分组
        SET_APIGROUP: (state, apiGroup) => {
            state.apiGroup = apiGroup
            localStorage.setItem('apiGroup', apiGroup)
        },

        //接口信息
        SET_INTERFACEINFO: (state, interfaceInfo) => {
            this.interfaceInfo = interfaceInfo
            localStorage.setItem('interfaceInfo', interfaceInfo)
        }
    }
})


export default store