import axios from 'axios'
// import NProgress from 'nprogress'
import {
    MessageBox,
    Message
} from 'element-ui'

// 创建axios实例
const service = axios.create({
    // `baseURL` 将自动加在 `url` 前面，除非 `url` 是一个绝对 URL。
    // 它可以通过设置一个 `baseURL` 便于为 axios 实例的方法传递相对 URL
    baseURL: '/dev-api',
    // `timeout` 指定请求超时的毫秒数(0 表示无超时时间)
    // 如果请求话费了超过 `timeout` 的时间，请求将被中断
    timeout: 5000,
})

//post请求设置
// axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=UTF-8';
// axios.defaults.headers.post['Content-Type'] = 'Content-Type: application/json';



//request 拦截
service.interceptors.request.use(config => {
    // 开启进度条
    // NProgress.start();
    return config

})


//response 拦截
service.interceptors.response.use(
    response => {
        const res = response.data
        if (res.code == 0 || res.code == 100 || res.code == 200 || res.code == 500) {

            // 关闭进度条
            // NProgress.done()
            return res

        } else {
            Message({
                message: res.message || 'Error',
                type: 'error',
                duration: 2 * 1000
            })
            if (res.code === 50008 || res.code === 50012 || res.code === 50014) {
                // to re-login
                MessageBox.confirm('You have been logged out, you can cancel to stay on this page, or log in again', 'Confirm logout', {
                    confirmButtonText: 'Re-Login',
                    cancelButtonText: 'Cancel',
                    type: 'warning'
                }).then(() => {
                    store.dispatch('user/resetToken').then(() => {
                        location.reload()
                    })
                })
            }
            // 关闭进度条
            // NProgress.done()
            return Promise.reject(new Error(res.message || 'Error'))
        }

        // 关闭进度条
        // NProgress.done()

    },
    error => {
        console.log('err' + error) // for debug
        Message({
            message: error.message,
            type: 'error',
            duration: 3 * 1000
        })

        // 关闭进度条
        // NProgress.done()
        return Promise.reject(error)
    }
)

export default service